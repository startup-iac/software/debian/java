#!/bin/bash

JAVA_VERSION=${JAVA_VERSION:-11}
JAVA_HOME="/usr/lib/jvm/temurin-$JAVA_VERSION-jdk-amd64"

# Set environment
echo "export JAVA_HOME=$JAVA_HOME" | sudo tee -a $ENVIRONMENT_FILE
source /etc/profile

# Add Java Repositories
wget -qO - https://packages.adoptium.net/artifactory/api/gpg/key/public | sudo gpg --dearmor -o $KEYRINGS_PATH/adoptium.gpg
echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=$KEYRINGS_PATH/adoptium.gpg] \
    https://packages.adoptium.net/artifactory/deb $(lsb_release -cs) main" \
    | sudo tee /etc/apt/sources.list.d/adoptium.list

# Update Repositories
sudo apt-get update

# Install Java
sudo apt-get install -y temurin-$JAVA_VERSION-jdk